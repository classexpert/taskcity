namespace TaskCity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrateDB : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaskCats", "Info", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TaskCats", "Info");
        }
    }
}
