﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TaskCity.Startup))]
namespace TaskCity
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
