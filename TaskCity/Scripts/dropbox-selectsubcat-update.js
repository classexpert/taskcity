﻿function AppendUrlParamTokens(url, params) {

    for (var param in params) {
        if (params[param] == null) {
            delete params[param];
        }
    }

    return url + "?" + jQuery.param(params);
}

function OnSelectedCatChange(ddl, url) {
    var d = document.getElementById("catinfo");
    d.innerHTML = '';
    d.hidden = true;
    jQuery.getJSON(AppendUrlParamTokens(url, { id: ddl.options[ddl.selectedIndex].value }), function (result) {
        var target = jQuery('#subcats_ddl');
        target.empty();
        jQuery(document.createElement('option'))
            .attr('value', -1)
            .text("Не выбрано")
            .appendTo(target);

        jQuery(result).each(function () {
            jQuery(document.createElement('option'))
                .attr('value', this.Value)
                .text(this.Text)
                .appendTo(target);
        });
    });
};

function UpdateCatInfoContent(ddl, url) {
    jQuery.getJSON(AppendUrlParamTokens(url, { id: ddl.options[ddl.selectedIndex].value }), function (result) {
        var d = document.getElementById("catinfo");
        if (result == '') {
            d.innerHTML = '';
            d.hidden = true;
        } else {
            d.innerHTML = result;
            d.hidden = false;
        }

    });
};