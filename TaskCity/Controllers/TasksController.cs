﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using TaskCity.Models;

namespace TaskCity.Controllers
{
    [Authorize]
    public class TasksController : Controller
    {
        private TaskContext db = new TaskContext();

        // GET: Tasks
        public async Task<ActionResult> Index()
        {
            return View(db.GetTasksForUser(User.Identity.GetUserId()));
        }

        // GET: Tasks/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WTask wTask = await db.Tasks.FindAsync(id);
            if (wTask == null)
            {
                return HttpNotFound();
            }
            return View(wTask);
        }

        [HttpGet]
        public JsonResult GetSubCats(int? id)
        {
            return Json(new SelectList(db.GetSubCatsOf(id.Value), "Id", "Name"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCatInfo(int? id)
        {
            if (id == null)
                return new JsonResult();
            string result = db.GetCatInfo(id.Value);
            if (result == null)
                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            return Json(db.GetCatInfo(id.Value), JsonRequestBehavior.AllowGet);
        }

        // GET: Tasks/Create
        public ActionResult Create()
        {
            ViewBag.CatItems = new SelectList(db.RootCats, "Id", "Name");
            ViewBag.SubCatItems = new SelectList(db.GetSubCatsOf(db.RootCats.ElementAt(0).Id), "Id", "Name");
            if (db.Tasks.Any())
            {
                var lastTask = db.Tasks.ToList().Last();
                var newTask = new WTask();
                newTask.Name = lastTask.Name;
                newTask.Phone = lastTask.Phone;
                newTask.Address = lastTask.Address;

                return View(newTask);
            }
            return View();
        }

        // POST: Tasks/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,UserId,CatId,SubCatId,Description,Address,Phone,Name")] WTask wTask)
        {
            if (ModelState.IsValid)
            {
                wTask.UserId = User.Identity.GetUserId();
                db.Tasks.Add(wTask);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CatItems = new SelectList(db.RootCats, "Id", "Name");
            ViewBag.SubCatItems = new SelectList(db.GetSubCatsOf(db.RootCats.ElementAt(0).Id), "Id", "Name");
            return View(wTask);
        }

        // GET: Tasks/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WTask wTask = await db.Tasks.FindAsync(id);
            if (wTask == null)
            {
                return HttpNotFound();
            }

            ViewBag.CatItems = new SelectList(db.RootCats, "Id", "Name");
            ViewBag.SubCatItems = new SelectList(db.GetSubCatsOf(db.RootCats.ElementAt(0).Id), "Id", "Name");

            return View(wTask);
        }

        // POST: Tasks/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,UserId,CatId,SubCatId,Description,Address,Phone,Name")] WTask wTask)
        {
            if (ModelState.IsValid)
            {
                wTask.UserId = User.Identity.GetUserId();
                db.Entry(wTask).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(wTask);
        }

        // GET: Tasks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WTask wTask = await db.Tasks.FindAsync(id);
            if (wTask == null)
            {
                return HttpNotFound();
            }
            return View(wTask);
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            WTask wTask = await db.Tasks.FindAsync(id);
            db.Tasks.Remove(wTask);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
