﻿using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using TaskCity.Models;

namespace TaskCity.Controllers
{
    public class HomeController : Controller
    {
        private readonly TaskContext db = new TaskContext();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ApplicationSignInManager SignInManager
        {
            get { return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>(); }
            private set { _signInManager = value; }
        }

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        public ActionResult Index(int catId = 0, int subCatId = 0)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Tasks");
            ViewBag.CatItems = new SelectList(db.RootCats, "Id", "Name");
            ViewBag.SubCatItems = new SelectList(db.GetSubCatsOf(db.RootCats.ElementAt(0).Id), "Id", "Name");
            var model = new IndexWTaskModel();
            model.CatId = catId;
            model.SubCatId = subCatId;
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(IndexWTaskModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser {UserName = model.Email, Email = model.Email};
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);
                await UserManager.SendEmailAsync(user.Id, "ololo", "pishpish");
                if (result.Succeeded)
                {
                    model.UserId = user.Id;
                    var rr = model.AsUserTask();
                    db.Tasks.Add(rr);
                    db.SaveChanges();
                    await SignInManager.SignInAsync(user, true, true);
                    
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.Message = result.Errors.First();
                return View("~/Views/Shared/CustomError.cshtml");
            }
            ViewBag.CatItems = new SelectList(db.RootCats, "Id", "Name");
            ViewBag.SubCatItems = new SelectList(db.GetSubCatsOf(db.RootCats.ElementAt(0).Id), "Id", "Name");
            return View(model);
        }


        [HttpGet]
        public JsonResult GetSubCats(int? id)
        {
            return Json(new SelectList(db.GetSubCatsOf(id.Value), "Id", "Name"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCatInfo(int? id)
        {
            if(id==null)
                return new JsonResult();
            string result = db.GetCatInfo(id.Value);
            if(result==null)
                return Json(string.Empty, JsonRequestBehavior.AllowGet);

            return Json(db.GetCatInfo(id.Value), JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Принцип работы.";

            return View();
        }

        public ActionResult Contact()
        {
            //ViewBag.Message = "Контакты для связи.";

            return View();
        }
    }
}