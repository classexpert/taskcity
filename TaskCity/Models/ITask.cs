﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskCity.Models
{
    public interface ITask
    {
        int CatId { get; set; }
        int SubCatId { get; set; }

        string Description { get; set; }
        string Address { get; set; }

        string Phone { get; set; }

        string Name { get; set; }

    }
}