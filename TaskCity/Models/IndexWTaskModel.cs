﻿using System.ComponentModel.DataAnnotations;

namespace TaskCity.Models
{
    public class IndexWTaskModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int CatId { get; set; }

        [Required(ErrorMessage = "Пожалуйста, выберите категорию")]
        public int SubCatId { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите описание")]
        public string Description { get; set; }

        public string Address { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите телефон")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите свое имя")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите email")]
        [EmailAddress(ErrorMessage = "Вы ввели некорректный email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} символов.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }


        public WTask AsUserTask()
        {
            var res = new WTask();
            res.Address = Address;
            res.CatId = CatId;
            res.Description = Description;
            res.Name = Name;
            res.Phone = Phone;
            res.SubCatId = SubCatId;
            res.UserId = UserId;

            return res;
        }
    }
}