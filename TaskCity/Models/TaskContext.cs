﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace TaskCity.Models
{
    public class TaskContext : DbContext
    {
        public TaskContext() : base("DefaultConnection")
        {}

        public DbSet<TaskCat> Cats { get; set; }

        public DbSet<WTask> Tasks { get; set; }

        public IEnumerable<TaskCat> RootCats
        {
            get { return GetSubCatsOf(0); }
        }

        public IEnumerable<TaskCat> GetSubCatsOf(int id)
        {
            return Cats.Where(e => e.SubCatId == id);
        }

        public string GetCatInfo(int id)
        {
            return Cats.First(e => e.Id == id).Info;
        }

        public IEnumerable<WTask> GetTasksForUser(string id)
        {
            return Tasks.Where(e=>e.UserId==id);
        }
    }
}