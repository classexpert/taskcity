﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskCity.Models
{
    public class TaskCat
    {
        public int Id { get; set; }

        public int SubCatId { get; set; }

        public string Name { get; set; }

        public string Info { get; set; }

        public TaskCat(string name, int subCatId = 0)
        {
            Name = name;
            SubCatId = subCatId;
        }

        public TaskCat()
        {
            Name = string.Empty;
            SubCatId = -1;
        }
    }
}