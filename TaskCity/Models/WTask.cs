﻿using System.ComponentModel.DataAnnotations;

namespace TaskCity.Models
{
    public class WTask : ITask
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int CatId { get; set; }

        [Required(ErrorMessage = "Пожалуйста, выберите категорию")]
        public int SubCatId { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите описание")]
        public string Description { get; set; }

        public string Address { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите телефон")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите свое имя")]
        public string Name { get; set; }
    }
}